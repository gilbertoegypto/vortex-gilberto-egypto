Esse desafio consiste na criação de um conversor de dígitos romanos para algarismos arábicos.
O programa foi criado em Javascript e HTML. 
Para seu uso só é preciso abrir o arquivo HTML e em seguida digitar os valores em dígitos romanos, 
sem espaço entre ou após os números, podendo ser em letra maiúscula ou minúscula, e em seguida clicar no botão "CONVERTER".
O maior valor que pode ser convertido nesse conversor é 3,999, equivalente a MMMCMXCIX em dígitos romanos, após isso 
é necessário utilizar um traço em cima das letras, que necessita um input específico que não é presente na maioria dos teclados.


Essa foi a submissão do aluno Gilberto Fábio Egypto de matricula 2116935.
