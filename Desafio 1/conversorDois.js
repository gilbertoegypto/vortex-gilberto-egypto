function convert(dig) {
    let valor = 0, digitos = [];
    digitos = dig.split('');


    if (dig * 0 == 0) {
        console.log("Erro, valor inválido");
        return "Erro, valor inválido";
    }
    for (i = 0; i < digitos.length; i++) {

        switch (digitos[i]) {
            case "I":
                if (digitos[i + 1] != null) {
                    if (digitos[i + 1] == "V" || digitos[i + 1] == "X") {
                        valor -= 1;
                    } else {
                        valor += 1;
                    }
                } else {
                    valor += 1;
                }
                break;
            case "V":
                valor += 5;
                break;
            case "X":
                if (digitos[i + 1] != null) {
                    if (digitos[i + 1] == "L" || digitos[i + 1] == "C") {
                        valor -= 10;
                    } else {
                        valor += 10;
                    }
                } else {
                    valor += 10;
                }
                break;
            case "L":
                valor += 50;
                break;
            case "C":
                if (digitos[i + 1] != null) {
                    if (digitos[i + 1] == "D" || digitos[i + 1] == "M") {
                        valor -= 100;
                    } else {
                        valor += 100;
                    }
                } else {
                    valor += 100;
                }
                break;
            case "D":
                valor += 500;
                break;
            case "M":
                valor += 1000;
                break;
            default:
                console.log("Erro, valor inválido");
                return "Erro, valor inválido";

        }


    }
    if (valor > 3999) {
        alert("O valor máximo que esse conversor pode converter é 3999, MMMCMXCIX em Romano.");
        console.log("Erro, valor inválido");
        return "Erro, valor inválido";
    }
    console.log(valor);
    return valor;
}


/*$(document).ready(function(){
    $("#submeter").on( "click", function(){
        

        let entrada = $("#digito").val().toUpperCase();
        $("#digito").val('')

        $("#result").val(convert(entrada))
        console.log(convert(entrada))

    } )
})*/

window.onload = function () {

    document.getElementById("submeter").onclick = function () {

        let entrada = document.getElementById("digito").value.toUpperCase();

        document.getElementById("digito").value = "";

        if (convert(entrada) != "Erro, valor inválido") {
            document.getElementById("result").value = `${entrada} = ${convert(entrada)}`;
        } else {
            document.getElementById("result").value = "Erro, valor inválido";
        }



    }

};