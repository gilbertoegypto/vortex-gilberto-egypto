Esse desafio consiste na de um algoritmo que irá gerar uma constelação com ligações entre as estrelas de forma aleatórias, com base em um INPUT com o número de estrelas que formam essa constelação. 
Em seguida o programa irá gerar uma Matriz representando essa constelação e as ligações entre as estrelas. Por fim o programa deve pegar as duas estrelas informadas pelo seu número, 
fornecido pelo usuário através de INPUT e informar se há ou não ligação entre essas duas estrelas.
O programa foi criado em Javascript e HTML. 
Para seu uso só é preciso abrir o arquivo HTML e em informar quantas estrelas compõe essa constelação, 
só sendo aceito constelações de no mínimo 4 e no máximo 8 estrelas. Após isso deve ser informado as duas através do seu número e apertar o botão "GERAR" para que o programa verifique se há ligação entre elas. 


Essa foi a submissão do aluno Gilberto Fábio Egypto de matricula 2116935.
