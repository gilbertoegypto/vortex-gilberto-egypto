function randomizar() {
    let booloean = Math.floor(Math.random() * 2);
    return booloean;
}

function criarConstelacao(estrelas) {
    let constelacao = new Array(estrelas);

    for (i = 0; i < estrelas; i++) {
        constelacao[i] = new Array(estrelas);
        for (j = 0; j < estrelas; j++) {
            constelacao[i][j] = "x"
        }
        constelacao[i][i] = 0;
    }

    for (i = 0; i < estrelas; i++) {

        for (j = 0; j < estrelas; j++) {
            if (constelacao[i][j] == "x") {
                constelacao[i][j] = randomizar();
                constelacao[j][i] = constelacao[i][j];
            }
        }
    }

    return constelacao;
}

function ligacao(constelacao, estrelaUm, estrelaDois) {

    console.log("Ligacao")
    console.log(constelacao[estrelaUm][estrelaDois])
    if (constelacao[estrelaUm][estrelaDois] == 1) {
        console.log("Há ligação")
        return "<br>há ligação"
    }
    return "<br>não há ligação"
}

function desenharMatriz(constelacao) {
    let matriz = "";
    for (i = 0; i < constelacao.length; i++) {
        for (j = 0; j < constelacao.length; j++) {
            //document.write(`| ${constelacao[i][j]} |`);
            matriz += `| ${constelacao[i][j]} |`;

        }
        //document.write("<br>")
        matriz += "<br>"
    }
    return matriz;
}

window.onload = function () {

    document.getElementById("submeter").onclick = function () {

        let entrada = document.getElementById("estrelas").value;
        let estrelaUm = document.getElementById("estrelaUm").value;
        let estrelaDois = document.getElementById("estrelaDois").value;
        let erro = false;

        document.getElementById("estrelas").value = "";
        document.getElementById("estrelaUm").value = "";
        document.getElementById("estrelaDois").value = "";

        if (entrada > 8 || entrada < 4) {
            alert("A constelação tem que ter entre 4 e 8 estrelas");
            console.log("A constelação tem que ter entre 4 e 8 estrelas");
            erro = true;
        }
        if (estrelaUm >= entrada || estrelaUm < 0 || estrelaDois == estrelaUm) {
            alert("O número da estrela é inválido");
            console.log("O número da estrela é inválido");
            erro = true;
        }
        if (estrelaDois >= entrada || estrelaDois < 0 || estrelaDois == estrelaUm) {
            alert("O número da estrela é inválido");
            console.log("O número da estrela é inválido");
            erro = true;
        }

        if (erro == false) {
            let constelacao = criarConstelacao(entrada);
            console.log("Matriz gerada");
            alert("Matriz gerada");
            console.log(constelacao);
            //document.write(desenharMatriz(constelacao));
            document.getElementById("matriz").innerHTML = `
            Constelação de ${entrada} estrelas: <br><br>
            ${desenharMatriz(constelacao)}
            ${ligacao(constelacao, estrelaUm, estrelaDois)} entre
            as estrelas ${estrelaUm} e ${estrelaDois}`
            //desenharMatriz(constelacao)+ligacao(constelacao,estrelaUm,estrelaDois)

        }

    }

};