Esse desafio consiste na criação de um algoritmo para gerar randomicamente um baralho, 
distribuir as cartas deste baralho entre dois jogadores robôs e a mesa e em seguida informar qual dos dois jogadores venceu a partida que segue regras baseado no jogo Poker Texas Hold’em.
O programa foi criado em Javascript e HTML. 
Para seu uso só é preciso abrir o arquivo HTML e em seguida clicar no botão “JOGAR”, o programa irá então mostrar na tela quais cartas foram sorteadas para a mesa e quais foram sorteadas para cada jogador, 
informando em seguida qual foi a jogada que cada jogador conseguiu montar com sua mão e por fim informando quem foi o vencedor daquela partida. O jogo pode ser repetido apenas com o pressionar do botão “JOGAR”. 


Essa foi a submissão do aluno Gilberto Fábio Egypto de matricula 2116935.

