
//chamar
function cartaAlta(mao, robo) {
    let maiorCarta = 0;
    let comb = { "jogador": robo, "mao": "Carta Alta", "ponto": 1, "maiorCarta": maiorCarta, "carta": null };

    for (let i = 0; i < mao.length; i++) {

        if (parseInt(mao[i].valor) > maiorCarta) {
            maiorCarta = mao[i].valor;
            comb.maiorCarta = mao[i].mostrar();
            comb.carta = mao[i];
        }
    }

    return comb;
}

function acharPares(maoUm) {
    let maoUmTratado = new Array(7);
    for (let i = 0; i < 7; i++) {
        maoUmTratado[i] = maoUm[i].carta;
    }
    const mao = maoUmTratado;
    const pares = mao.filter((par, carta) => carta !== mao.indexOf(par));

    return pares;
}

//chamado pelo combinacoes
function pares(maoUm, robo) {
    let par = 0, trio = 0, quadra = 0;
    let maoUmTratado = new Array(7);
    for (let i = 0; i < 7; i++) {
        maoUmTratado[i] = maoUm[i].carta;
    }
    const mao = maoUmTratado;
    const pares = mao.filter((par, carta) => carta !== mao.indexOf(par));
    for (let i = 0; i < pares.length; i++) {
        let qtd = 0;
        for (let j = 0; j < maoUmTratado.length; j++) {
            if (pares[i] == maoUmTratado[j]) {
                if (pares[i] != pares[i - 1]){
                    qtd += 1;
                }  
            }
        }
        if (qtd == 2) {
            par += 1;
        } else if (qtd == 3) {
            trio += 1;
        } else if (qtd == 4) {
            quadra += 1;
        }
    }

    return { "jogador": robo, "par": par, "trio": trio, "quadra": quadra };
}
//chamar
function combinacoes(mao, robo) {

    let comb = { "jogador": robo, "mao": "Carta Alta" };

    if ((pares(mao, robo)).par == 1) {
        if ((pares(mao, robo)).trio >= 1) {

            comb.mao = "Full House";
        } else {

            comb.mao = "Par";
        }
    } else if ((pares(mao, robo)).par > 1) {
        if ((pares(mao, robo)).trio >= 1) {

            comb.mao = "Full House";
        } else {

            comb.mao = "Dois Pares";
        }

    } else if ((pares(mao, robo)).trio == 1) {

        comb.mao = "Trio";

    } else if ((pares(mao, robo)).trio > 1) {

        comb.mao = "Full House";
    } else if ((pares(mao, robo)).quadra == 1) {

        comb.mao = "Quadra";
    }
    return comb;
}
//chamado pelo straight
function ordernarVetor(V) {
    let aux;
    for (let i = 0; i < V.length - 1; i++) {
        for (let j = 0; j < V.length - 1 - i; j++) {
            if (V[j].valor > V[j + 1].valor) {
                aux = V[j];
                V[j] = V[j + 1];
                V[j + 1] = aux;
            }
        }
    }
    return V;
}
//chamar
function straight(mao, robo) {
    
    let comb = { "jogador": robo, "mao": "Carta Alta" , "cartas": [] };
    let maoOrdenada = ordernarVetor(mao);
    let cont = 0;
   for (let i = 0; i < maoOrdenada.length - 1; i++) {
        if (maoOrdenada[i + 1].valor - maoOrdenada[i].valor == 1) {
            cont += 1;
            comb.cartas.push(maoOrdenada[i].mostrar());
        } else if (maoOrdenada[i + 1].valor - maoOrdenada[i].valor == 0) {

        } else if (maoOrdenada[i + 1].valor - maoOrdenada[i].valor > 1) {
            cont = 0;
            comb.cartas = [];
        }
        if (cont + 1 == 4) {
           comb.cartas.push(maoOrdenada[i+1].mostrar());
            if (maoOrdenada[i + 1].valor == 13) {
                if (maoOrdenada[0].valor == 1) {
                    comb.mao = "Straight";
                    comb.cartas.push(maoOrdenada[0].mostrar());
                    break;
                }
            }
        }
        if (cont + 1 == 5) {
            comb.mao = "Straight";
            comb.cartas.push(maoOrdenada[i+1].mostrar());
            break;
        }
    }

    // for (let j = 0; j < maoOrdenada.length; j++) {
    //     comb.cartas.push(maoOrdenada[j].mostrar());
    // }
    return comb; 

}
//chamado pelo flush
function straightFlush(mao) {
    
    let comb = { "mao": "Flush", "cartas": [] };
    let maoOrdenada = ordernarVetor(mao);
    let cont = 0;
    for (let i = 0; i < maoOrdenada.length - 1; i++) {
        if (maoOrdenada[i + 1].valor - maoOrdenada[i].valor == 1) {
            cont += 1;
        } else if (maoOrdenada[i + 1].valor - maoOrdenada[i].valor == 0) {

        } else if (maoOrdenada[i + 1].valor - maoOrdenada[i].valor > 1) {
            cont = 0;
        }
        if (cont + 1 == 4) {
            if (maoOrdenada[i + 1].valor == 13) {
                if (maoOrdenada[0].valor == 1) {
                    comb.mao = "Royal Flush";
                    break;
                }
            }
        }
        if (cont + 1 == 5) {
            comb.mao = "Straight Flush";
            break;
        }

    }

    for (let j = 0; j < maoOrdenada.length; j++) {
        comb.cartas.push(maoOrdenada[j].mostrar());
    }


    return comb;
}
//chamar
function flush(mao, robo) {
  
    let ouro = [], paus = [], copas = [], espada = [];
    let comb = { "jogador": robo, "mao": "Carta Alta", "cartas": [] };

    for (let i = 0; i < 7; i++) {
        switch (mao[i].naipe) {
            case "C": copas.push(mao[i]); break;
            case "E": espada.push(mao[i]); break;
            case "O": ouro.push(mao[i]); break;
            case "P": paus.push(mao[i]); break;
            default: console.log(`erro func flush. carta: ${mao}, posição ${i} naipe: ${mao[i][1]}`); break;
        }

    }


    if (ouro.length > 4 || paus.length > 4 || copas.length > 4 || espada.length > 4) {


        if (ouro.length > paus.length && ouro.length > copas.length && ouro.length > espada.length) {
            comb.mao = straightFlush(ouro).mao;
            comb.cartas = straightFlush(ouro).cartas;
        } else if (paus.length > ouro.length && paus.length > copas.length && paus.length > espada.length) {
            comb.mao = straightFlush(paus).mao;
            comb.cartas = straightFlush(paus).cartas;
        } else if (copas.length > paus.length && copas.length > ouro.length && copas.length > espada.length) {
            comb.mao = straightFlush(copas).mao;
            comb.cartas = straightFlush(copas).cartas;
        } else if (espada.length > paus.length && espada.length > copas.length && espada.length > ouro.length) {
            comb.mao = straightFlush(espada).mao;
            comb.cartas = straightFlush(espada).cartas;
        }


    }

    return comb;

}
