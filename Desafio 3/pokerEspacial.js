

class Cartas {
    constructor(carta, naipe, valor) {
        this.carta = carta;
        this.naipe = naipe;
        this.valor = valor;
    }

    mostrar() {
        return `${this.carta}${this.naipe}`;
    }
}

function criarBaralho() {
    let baralho = new Array(4);

    for (let i = 0; i < 4; i++) {
        baralho[i] = new Array(13);
    }

    for (let i = 0; i < 4; i++) {


        for (let j = 0; j < 13; j++) {
            baralho[i][j] = new Cartas();
            if (j == 0) {
                baralho[i][j].carta = "A";
                baralho[i][j].valor = 1;
            } else if (j == 10) {
                baralho[i][j].carta = "J";
                baralho[i][j].valor = 11;
            } else if (j == 11) {
                baralho[i][j].carta = "Q";
                baralho[i][j].valor = 12;
            } else if (j == 12) {
                baralho[i][j].carta = "K";
                baralho[i][j].valor = 13;
            } else {
                baralho[i][j].carta = (j + 1).toString();
                baralho[i][j].valor = parseInt(j + 1);
            }
            if (i == 0) {
                baralho[i][j].naipe = "C";
            } else if (i == 1) {
                baralho[i][j].naipe = "E";
            } else if (i == 2) {
                baralho[i][j].naipe = "O";
            } else if (i == 3) {
                baralho[i][j].naipe = "P";
            }


        }
    }
    console.log(baralho);
    return baralho;
}

function distribuir(baralho) {

    let jogo = new Array(3);

    for (let i = 0; i < 3; i++) {
        let naipe;
        let numero;
        if (i == 0) {
            jogo[i] = new Array(5);
            for (let j = 0; j < 5; j++) {
                naipe = Math.floor(Math.random() * 4);
                numero = Math.floor(Math.random() * 13);
                let carta = baralho[naipe][numero];
                while (carta == null) {
                    naipe = Math.floor(Math.random() * 4);
                    numero = Math.floor(Math.random() * 13);
                    carta = baralho[naipe][numero];
                }
                jogo[i][j] = carta
                baralho[naipe][numero] = null;
            }
        } else {
            jogo[i] = new Array(2);
            for (let j = 0; j < 2; j++) {
                naipe = Math.floor(Math.random() * 4);
                numero = Math.floor(Math.random() * 13);
                let carta = baralho[naipe][numero];
                while (carta == null) {
                    naipe = Math.floor(Math.random() * 4);
                    numero = Math.floor(Math.random() * 13);
                    carta = baralho[naipe][numero];
                }
                jogo[i][j] = carta;
                baralho[naipe][numero] = null;
            }
        }
    }

    document.getElementById("mesa").innerHTML = `As cartas na mesa são 
    ${jogo[0][0].mostrar()} ${jogo[0][1].mostrar()} ${jogo[0][2].mostrar()} ${jogo[0][3].mostrar()} ${jogo[0][4].mostrar()}`;

    document.getElementById("maoUm").innerHTML = `As cartas do Robô Um são 
    ${jogo[1][0].mostrar()} ${jogo[1][1].mostrar()}`;

    document.getElementById("maoDois").innerHTML = `As cartas do Robô Dois são 
    ${jogo[2][0].mostrar()} ${jogo[2][1].mostrar()}`;

    return jogo;
}

function poker(cartas) {

    let roboUm = new Array(7), roboDois = new Array(7);

    for (let i = 0; i < 7; i++) {
        if (i < 5) {
            roboUm[i] = cartas[0][i];
            roboDois[i] = cartas[0][i];
        } else if (i == 5) {
            roboUm[i] = cartas[1][0];
            roboDois[i] = cartas[2][0];
        } else if (i == 6) {
            roboUm[i] = cartas[1][1];
            roboDois[i] = cartas[2][1];
        }
    }



    console.log(jogadas(roboUm, roboDois));

}

//chamado por jogadas
function pontuacao(mao, jogador) {

    let comb = { "jogador": jogador, "mao": "Carta Alta", "ponto": 1, "cartas": [] };

    if (flush(mao, jogador).mao == "Royal Flush") {
        comb.mao = "Royal Flush";
        comb.ponto = 10;
        comb.cartas = flush(mao, jogador).cartas;
    } else if (flush(mao, jogador).mao == "Straight Flush") {
        comb.mao = "Straight Flush";
        comb.ponto = 9;
        comb.cartas = flush(mao, jogador).cartas;
    } else if (combinacoes(mao, jogador).mao == "Quadra") {
        //quadra
        comb.mao = "Quadra";
        comb.ponto = 8;
    } else if (combinacoes(mao, jogador).mao == "Full House") {
        //Full house
        comb.mao = "Full House";
        comb.ponto = 7;
    } else if (flush(mao, jogador).mao == "Flush") {
        comb.mao = "Flush";
        comb.ponto = 6;
        comb.cartas = flush(mao, jogador).cartas;
    } else if (straight(mao, jogador).mao == "Straight") {
        //straight
        comb.mao = "Straight";
        comb.ponto = 5;
        comb.cartas = straight(mao, jogador).cartas;
    } else if (combinacoes(mao, jogador).mao == "Trio") {
        //trio
        comb.mao = "Trio";
        comb.ponto = 4;
    } else if (combinacoes(mao, jogador).mao == "Dois Pares") {
        //dois pares
        comb.mao = "Dois Pares";
        comb.ponto = 3;
    } else if (combinacoes(mao, jogador).mao == "Par") {
        //par
        comb.mao = "Par";
        comb.ponto = 2;
    }


    return comb;
}

//chamado pelo jogadas
function mostrarCartas(mao, jogador, pontuacao) {
    let maoOrdenada = ordernarVetor(mao);
    let mostrarMao = [];

    if (pontuacao.mao == "Royal Flush") {
        mostrarMao = pontuacao.cartas;

    } else if (pontuacao.mao == "Straight Flush") {
        mostrarMao = pontuacao.cartas;

    } else if (pontuacao.mao == "Quadra") {
        let pares = acharPares(maoOrdenada);
        for (let i = 0; i < maoOrdenada.length; i++) {
            if (pares.includes(maoOrdenada[i].carta)) {
                mostrarMao.push(maoOrdenada[i].mostrar());
            }
            if (mostrarMao.length == 4) {
                break;
            }
        }

    } else if (pontuacao.mao == "Full House") {
        let pares = acharPares(maoOrdenada);
        for (let i = 0; i < maoOrdenada.length; i++) {
            if (pares.includes(maoOrdenada[i].carta)) {
                mostrarMao.push(maoOrdenada[i].mostrar());
            }
            if (mostrarMao.length == 5) {
                break;
            }
        }

    } else if (pontuacao.mao == "Flush") {
        mostrarMao = pontuacao.cartas;

    } else if (pontuacao.mao == "Straight") {

        mostrarMao = pontuacao.cartas;

    } else if (pontuacao.mao == "Trio") {
        let pares = acharPares(maoOrdenada);
        for (let i = 0; i < maoOrdenada.length; i++) {
            if (pares.includes(maoOrdenada[i].carta)) {
                mostrarMao.push(maoOrdenada[i].mostrar());
            }
            if (mostrarMao.length == 3) {
                break;
            }
        }

    } else if (pontuacao.mao == "Dois Pares") {
        let pares = acharPares(maoOrdenada);
        for (let i = 0; i < maoOrdenada.length; i++) {
            if (pares.includes(maoOrdenada[i].carta)) {
                mostrarMao.push(maoOrdenada[i].mostrar());
            }
            if (mostrarMao.length == 4) {
                break;
            }
        }

    } else if (pontuacao.mao == "Par") {
        let pares = acharPares(mao);
        for (let i = 0; i < maoOrdenada.length; i++) {
            if (pares.includes(maoOrdenada[i].carta)) {
                mostrarMao.push(maoOrdenada[i].mostrar());
            }
            if (mostrarMao.length == 2) {
                break;
            }
        }

    } else if (pontuacao.mao == "Carta Alta") {
        mostrarMao.push(maoOrdenada[maoOrdenada.length - 1].mostrar());

    }

    if (jogador == "Robô Um") {

        document.getElementById("jogadaUm").innerHTML = `As jogada do Robô Um foi 
            <b>${pontuacao.mao}</b>`;
        document.getElementById("cartasUm").innerHTML = `${mostrarMao}`;

    } else if (jogador == "Robô Dois") {

        document.getElementById("jogadaDois").innerHTML = `As jogada do Robô Dois foi 
            <b>${pontuacao.mao}</b>`;
        document.getElementById("cartasDois").innerHTML = `${mostrarMao}`;

    }

    console.log(`As cartas do jogador ${jogador} são: ${mostrarMao}`);

}



//chamado por poker
function jogadas(maoUm, maoDois) {

    let vencedor;

    mostrarCartas(maoUm, "Robô Um", pontuacao(maoUm, "Robô Um"));
    mostrarCartas(maoDois, "Robô Dois", pontuacao(maoDois, "Robô Dois"));

    console.log(pontuacao(maoUm, "Robô Um"));
    console.log(pontuacao(maoDois, "Robô Dois"));


    if (pontuacao(maoUm, "Robô Um").ponto == pontuacao(maoDois, "Robô Dois").ponto) {
        if (pontuacao(maoUm, "Robô Um").ponto == 1 && pontuacao(maoDois, "Robô Dois").ponto == 1) {
            if (cartaAlta(maoUm, "Robô Um").carta.valor > cartaAlta(maoDois, "Robô Dois").carta.valor) {
                vencedor = `Robô um vence`;
            } else if (cartaAlta(maoUm, "Robô Um").carta.valor < cartaAlta(maoDois, "Robô Dois").carta.valor) {
                vencedor = `Robô dois vence`;
            } else {
                vencedor = `Empate`;
            }
        } else {
            vencedor = `Empate`;
        }
    } else if (pontuacao(maoUm, "Robô Um").ponto > pontuacao(maoDois, "Robô Dois").ponto) {
        vencedor = `Robô um vence`;
    } else if (pontuacao(maoUm, "Robô Um").ponto < pontuacao(maoDois, "Robô Dois").ponto) {
        vencedor = `Robô dois vence`;
    }

    document.getElementById("vencedor").innerHTML = `${vencedor}!`;

    return vencedor;

}



window.onload = function () {

    document.getElementById("jogar").onclick = function () {

        poker(distribuir(criarBaralho()));


        //debug
        // for(let i=0;i<1000;i++){
        //     poker(distribuir(criarBaralho()));
        // };
    }

};